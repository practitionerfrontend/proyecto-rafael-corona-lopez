import { LitElement, html } from 'lit-element';
import sandbox from '../sandbox/sandbox.js';
class PersonaDialogo extends LitElement {
    

    static get properties() {
        return {
            titulo: {
                type: String
            },
            cancelButtonLabel: {type: String},
            acceptButtonLabel: {type: String}
        };
    }

    constructor() {
        super();
        this.titulo = '';
        this.cancelButtonLabel = '';
        this.acceptButtonLabel = '';
    }

    acceptButtonAction() {
        this.classList.remove("d-none");
        sandbox.dispatch('accept-action', {}, this);
    }
    cancelButtonAction() {
        this.classList.remove("d-none");
        sandbox.dispatch('cancel-action', {}), this;
    }
    render() {
        return html`
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">	
        <div>
            <div class="alert alert-info">Soy un titulo</div>
            <div>
                <button class="button button-primary"@click="${this.acceptButtonAction}">Aceptar</button>
                <button class="button button-secondary" @click="${this.cancelButtonAction}">Cancelar</button>
            </div>
        </div>
        `;
    }
}

customElements.define('persona-dialogo', PersonaDialogo)
