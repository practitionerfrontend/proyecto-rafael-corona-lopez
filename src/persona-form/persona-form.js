import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {
    

    static get properties() {
        return {
            person: {type: String},
        };
    }

    constructor() {
        super();
        this.person = {}
    }

    updateName(e) {
        this.person.name = e.target.value;
    }
    updateProfile(e) {
        this.person.profile = e.target.value;
    }
    updateYearsInCompany(e) {
        this.person.yearsInCompany = e.target.value;
    }


    goBack(e) { 
        e.preventDefault();	
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));	
    }

    guardarPersona(e) {
        e.preventDefault();
        this.person.photo = {
            "src": "./img/persona.jpg",
            "alt": "Persona"
        };
        this.dispatchEvent(new CustomEvent("persona-form-store",{
            detail: {
                person:  {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    }
                }
            })
        );
        e.preventDefault();
    }

    render() {
        return html`
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">	
        <div>
            <form>
            <div class="form-group">
						<label>Nombre Completo</label>
						<input @input="${this.updateName}"type="text" id="personFormName" class="form-control" placeholder="Nombre Completo"/>
					<div>
					<div class="form-group">
						<label>Perfil</label>
						<textarea @input="${this.updateProfile}"class="form-control" placeholder="Perfil" rows="5"></textarea>
					<div>
					<div class="form-group">
						<label>Años en la empresa</label>
						<input @input="${this.updateYearsInCompany}"type="text" class="form-control" placeholder="Años en la empresa"/>
					<div>
					<button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
					<button @click="${this.guardarPersona}" class="btn btn-success"><strong>Guardar</strong></button>
				
            </form>
        </div>
        `;
    }
}

customElements.define('persona-form', PersonaForm)
