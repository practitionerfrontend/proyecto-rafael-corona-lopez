import { LitElement, html } from 'lit-element';

class PersonaSidebar extends LitElement {
    

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();
    }

    newPerson() {
        debugger;
        this.dispatchEvent(new CustomEvent('new-person', {}));
    }

    render() {
        return html`
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">	
        <aside>
            <section>
            <div class="mt-5">
						<button class="w-100 btn bg-success" @click="${this.newPerson}" class="w-100 btn bg-success" style="font-size: 50px"><strong>+</strong></button>
					<div>
            </section>
        </aside>
        
        `;
    }
}

customElements.define('persona-sidebar', PersonaSidebar)
