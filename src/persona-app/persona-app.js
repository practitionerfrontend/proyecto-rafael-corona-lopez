import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-form/persona-form.js';
import '../persona-dialogo/persona-dialogo.js';
import '../sandbox/sandbox.js';

class PersonaApp extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();
    }

    newPerson(e) {
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    render() {
        return html`
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">	
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
				<persona-main class="col-10"></persona-main>
            </div>
            <persona-footer></persona-footer>
        `;
    }
}

customElements.define('persona-app', PersonaApp)