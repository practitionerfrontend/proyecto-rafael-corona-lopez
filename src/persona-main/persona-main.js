import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {
    

    static get properties() {
        return {
            people: {type: Array},
		    showPersonForm: {type: Boolean}
        };
    }

    constructor() {
        super();

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Ellen Ripley"
                }, 
                profile: "Lorem ipsum dolor sit amet."
            }, {
                name: "Bruce Banner",
                yearsInCompany: 2,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Bruce Banner"
                }, 
                profile: "Lorem ipsum."
            }, {
                name: "Éowyn",
                yearsInCompany: 5,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Éowyn"
                }, 
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
            }, {
                name: "Turanga Leela",
                yearsInCompany: 9,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Turanga Leela"
                }, 
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit."
            }, {
                name: "Tyrion Lannister",
                yearsInCompany: 1,
                photo: {
                    "src": "./img/persona.jpg",
                    "alt": "Tyrion Lannister"
                }, 
                profile: "Lorem ipsum."
            }
        ];
        this.showPersonForm = false;
    }

    deletePersone(e) { 
        console.log("deletePerson en persona-main");
        this.people = this.people.filter(
            person => person.name != e.detail.nombre
        );
    }

    updated(changedProperties) { 
        if (changedProperties.has("showPersonForm")) {
            this.showPersonForm ? this.showPersonFormData() : this.showPersonList();
        }
    }

    showPersonList() {
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");	
    }  
    showPersonFormData() {
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");	
    }
    personFormClose() {
        this.showPersonForm = false;
    }
    guardaPersona(e) {
        this.people.push(e.detail.person);
        this.showPersonForm = false;
    }

    render() {
        return html`
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">	
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html`<persona-ficha-listado 
                                        fname="${person.name}" 
                                        yearsInCompany="${person.yearsInCompany}"
                                        profile="${person.profile}"
                                        .photo="${person.photo}"
                                        @delete-person="${this.deletePersone}"
                                    >
                                </persona-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
            <persona-form id="personForm" @persona-form-store = "${this.guardaPersona}" class="d-none border rounded border-primary" @persona-form-close="${this.personFormClose}">
            </persona-form>
        </div>
        `;
    }
}

customElements.define('persona-main', PersonaMain)
