const sandbox = {

    on(event, callback) {

        Document.addEventListener(event, (e) => callback(e.detail))
    },
    dispatch(event, data, ctx) {
        ctx = ctx || document;
        ctx.dispatchEvent(new CustomEvent(event, {detail: data, bubbles: true, composed: true}))
    }
}

export default sandbox;

